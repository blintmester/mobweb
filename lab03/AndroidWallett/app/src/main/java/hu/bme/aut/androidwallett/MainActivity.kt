package hu.bme.aut.androidwallett

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import hu.bme.aut.androidwallett.databinding.ActivityMainBinding
import hu.bme.aut.androidwallett.databinding.SalaryRowBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var rowBinding: SalaryRowBinding
    private var sum = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.saveButton.setOnClickListener {
            if (binding.salaryName.text.toString().isEmpty() || binding.salaryAmount.text.toString().isEmpty()) {
                Snackbar.make(binding.root, R.string.warn_message, Snackbar.LENGTH_LONG).show()
                return@setOnClickListener
            }

            rowBinding = SalaryRowBinding.inflate(layoutInflater)

            rowBinding.salaryDirectionIcon.setImageResource(if (binding.expenseOrIncome.isChecked) R.drawable.expense else R.drawable.income)
            rowBinding.rowSalaryName.text = binding.salaryName.text.toString()
            rowBinding.rowSalaryAmount.text = binding.salaryAmount.text.toString()

            binding.listOfRows.addView(rowBinding.root)


            if (binding.expenseOrIncome.isChecked) {
                sum -= binding.salaryAmount.text.toString().toInt()
            } else {
                sum += binding.salaryAmount.text.toString().toInt()
            }
            binding.summary.visibility = VISIBLE
            binding.summary.text = "Balance: " + sum.toString()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.delete_all -> {
                binding.listOfRows.removeAllViews()
                binding.summary.visibility = INVISIBLE
                sum = 0
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}
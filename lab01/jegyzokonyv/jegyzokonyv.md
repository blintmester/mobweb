# Feladatok:

## 1.  Az új alkalmazást futtassák emulátoron (akinek saját készüléke van, az is próbálja ki)!

![](pics/01.png)

## 2.  Helyezzenek breakpointot a kódba, és debug módban indítsák az alkalmazást! (Érdemes megyfigyelni, hogy most másik Gradle Task fut a képernyő alján.)

![](pics/02.png)

## 3.  Indítsanak hívást és küldjenek SMS-t az emulátorra! Mit tapasztalnak?

![](pics/03.png)

Emulál egy hívást/sms küldést.

## 4.  Indítsanak hívást és küldjenek SMS-t az emulátorról! Mit tapasztalnak?

SMS elmegy, hívást autómatikusan felveszik.

## 5.  Tekintse át az Android Profiler nézet funkcióit a laborvezető segítségével!

![](pics/05.png)

## 6.  Változtassa a készülék pozícióját az emulátor megfelelő paneljének segítségével!

![](pics/06.png)


## 7.  Vizsgálja meg az elindított `HelloWorld` projekt nyitott szálait, memóriafoglalását!

![](pics/07a.png)
![](pics/07b.png)

## 8.  Vizsgálja meg a Logcat panel tartalmát!

![](pics/08.png)

## 9.  Vizsgálja meg az Analyze -> Inspect code eredményét!

![](pics/09.png)


10.  Keresse ki a létrehozott `HelloWorld` projekt mappáját és a build könyvtáron belül vizsgálja meg az `.apk` állomány tartalmát! Hol található a lefordított kód? 

![](pics/10.png)
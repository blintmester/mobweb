class Todo {
    constructor(name, state) {
        this.name = name;
        this.state = state;
    }
}

const todos = [];
const states = ["active", "inactive", "done"];
const tabs = ["all"].concat(states);

console.log(tabs);


const form = document.getElementById("new-todo-form");
const input = document.getElementById("new-todo-title");

form.onsubmit = event => {
    event.preventDefault(); // meggátoljuk az alapértelmezett működést, ami frissítené az oldalt
    if (input?.value?.length) { // ha érvényes érték van benne -- ekvivalens ezzel: if (input && input.value && input.value.length) vagy if (input != null && input.value != null && input.value.length > 0)
        todos.push(new Todo(input.value, "active")); // új to-do-t aktív állapotban hozunk létre
        input.value = ""; // kiürítjük az inputot
        saveTodos();
        renderTodos();
    }
}


class Button {
    constructor(action, icon, type, title, viewOnlyAllTabs = false) {
        this.action = action; // a művelet, amit a gomb végez
        this.icon = icon; // a FontAwesome ikon neve (class="fas fa-*")
        this.type = type; // a gomb Bootstrapbeni típusa ("secondary", "danger" stb.)
        this.title = title; // a gomb tooltip szövege
        this.viewOnlyAllTabs = viewOnlyAllTabs; //
    }
}

const buttons = [ // a gombokat reprezentáló modell objektumok tömbje
    new Button("done", "check", "success", "Mark as done"),
    new Button("active", "plus", "secondary", "Mark as active"),
    new Button("inactive", "minus", "secondary", "Mark as active"),
    new Button("remove", "trash", "danger", "Remove"),
    new Button("up", "arrow-up", "secondary", "Up", true),
    new Button("down", "arrow-down", "secondary", "Down", true),
];


function createElementFromHTML(html) {
    const virtualElement = document.createElement("div");
    virtualElement.innerHTML = html;
    return virtualElement.childElementCount == 1 ? virtualElement.firstChild : virtualElement.children;
}

function switchero(from, movedBy) {
    let fromIndex = todos.indexOf(from);
    let to = todos[fromIndex - movedBy];
    todos[fromIndex - movedBy] = from;
    todos[fromIndex] = to;
}


function renderTodos() {
    const todoList = document.getElementById("todo-list"); // megkeressük a konténert, ahová az elemeket tesszük
    todoList.innerHTML = ""; // a jelenleg a DOM-ban levő to-do elemeket töröljük
    todos.filter(todo => ["all", todo.state].includes(currentTab)).forEach(todo => {
        const row = createElementFromHTML(
            `<div class="row">
                <div class="col d-flex p-0">
                    <a class="list-group-item flex-grow-1" href="#">
                        ${todo.name}
                    </a>
                    <div class="btn-group action-buttons"></div>
                </div>
            </div>`);
        buttons.filter(button => !button.viewOnlyAllTabs || currentTab === "all").forEach(button => { // a gomb modellek alapján legyártjuk a DOM gombokat
            const btn = createElementFromHTML(
                `<button class="btn btn-outline-${button.type} fas fa-${button.icon}" title="${button.title}"></button>`
            );
            if (todo.state === button.action)
                btn.disabled = true;

            if ((todos.indexOf(todo) === 0 && button.action === "up") ||
                (todos.indexOf(todo) === todos.length -1 && button.action === "down")) {
                    btn.disabled = true;
            }

            btn.onclick = () => { // klikk eseményre
                switch(button.action) {
                    case "remove":
                        if (confirm("Are you sure you want to delete the todo titled '" + todo.name + "'?")) { // megerősítés után
                            todos.splice(todos.indexOf(todo), 1);
                        }
                        break;
                    case "up":
                        switchero(todo, 1);
                        break;
                    case "down":
                        switchero(todo, -1);
                        break;
                    default:
                        todo.state = button.action;
                        break;
                }
                saveTodos();
                renderTodos();
            }

            row.querySelector(".action-buttons").appendChild(btn); // a virtuális elem gomb konténerébe beletesszük a gombot
        });

        todoList.appendChild(row); // az összeállított HTML-t a DOM-ban levő #todo-list elemhez fűzzük
    });


    document.querySelector(".todo-tab[data-tab-name='all'] .badge").innerHTML = todos.length || "";

    for (let state of states)
        document.querySelector(`.todo-tab[data-tab-name='${state}'] .badge`).innerHTML = todos.filter(t => t.state === state).length || "";
}

renderTodos(); // kezdeti állapot kirajzolása


let currentTab; // a jelenleg kiválasztott fül

function selectTab(type) {
    currentTab = type; // eltároljuk a jelenlegi fül értéket
    for (let tab of document.getElementsByClassName("todo-tab")) {
        tab.classList.remove("active"); // az összes fülről levesszük az .active osztályt
        if (tab.getAttribute("data-tab-name") == type) // ha ez a fül van épp kiválasztva
            tab.classList.add("active"); // erre az egyre visszatesszük az .active osztályt
    }

    renderTodos(); // újrarajzolunk mindent
}

function saveTodos() {

    localStorage.setItem("todos", JSON.stringify(todos));
}
function loadTodos() {

    let tmp = JSON.parse(localStorage.getItem("todos"))
    tmp?.forEach(element => {todos.push(element);});
}

loadTodos();
selectTab("all"); // app indulásakor kiválasztjuk az "all" fület
